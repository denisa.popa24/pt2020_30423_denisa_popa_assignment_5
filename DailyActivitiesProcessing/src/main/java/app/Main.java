package app;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.stream.Stream;

import ControlLayer.Tasks;
import bussinesLayer.MonitoredData;
import bussinesLayer.MyData;


public class Main {

    public static void main(String[] args) {

        ArrayList<MonitoredData> activityList = new ArrayList<>();

        String fileName = "D:\\Assigment5PT2020\\pt2020_30423_denisa_popa_assignment_5\\DailyActivitiesProcessing\\out\\artifacts\\DailyActivitiesProcessing_jar\\Activities.txt";
        try {
            BufferedWriter bw1 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("Task_1.txt")));
            System.out.println("Task1 processing- result in Task_1.txt\n");
            bw1.write("Task 1 output : " + "\n");
            // read file into stream
            try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

                Iterator<String> myIterator = stream.iterator();
                while (myIterator.hasNext()) {
                    String currentString = myIterator.next();

                    String[] token = currentString.split("\\t+");
                    String startDateStr = token[0];
                    String endDateStr = token[1];
                    String activity = token[2];

                    String[] startDateTokens = startDateStr.split("\\s+");

                    StringTokenizer startDateStrT = new StringTokenizer(startDateTokens[0], "-");
                    int year = Integer.parseInt(startDateStrT.nextToken());
                    int month = Integer.parseInt(startDateStrT.nextToken());
                    int day = Integer.parseInt(startDateStrT.nextToken());

                    StringTokenizer startTime = new StringTokenizer(startDateTokens[1], ":");
                    int hour = Integer.parseInt(startTime.nextToken());
                    int minute = Integer.parseInt(startTime.nextToken());
                    int second = Integer.parseInt(startTime.nextToken());

                    MyData startDate = new MyData( year, month,day, hour, minute, second);

                    String[] endDateTokens = endDateStr.split("\\s+");
                    StringTokenizer endDateST = new StringTokenizer(endDateTokens[0], "-");

                    int yearEnd = Integer.parseInt(endDateST.nextToken());
                    int monthEnd = Integer.parseInt(endDateST.nextToken());
                    int dayEnd = Integer.parseInt(endDateST.nextToken());

                    StringTokenizer endTimeST = new StringTokenizer(endDateTokens[1], ":");
                    int hourEnd = Integer.parseInt(endTimeST.nextToken());
                    int minuteEnd = Integer.parseInt(endTimeST.nextToken());
                    int secondEnd = Integer.parseInt(endTimeST.nextToken());

                    MyData endDate = new MyData( yearEnd, monthEnd,dayEnd, hourEnd, minuteEnd, secondEnd);

                    MonitoredData monitoredData = new MonitoredData(startDate, endDate, activity);

                    activityList.add(monitoredData);

                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            for (MonitoredData currentData : activityList) {
                bw1.write(currentData.getStartTime().data() + currentData.getEndTime().data()
                        + currentData.getActivity() + "\n");
            }
            bw1.close();

            Tasks tasks = new Tasks();
            BufferedWriter bw2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("Task_2.txt")));
            ArrayList<Integer> dayList = tasks.countDays(activityList);
            bw2.write("Task 2 output : " + dayList.size() + " distinct days");
            System.out.println("Task2 processing ->result in Task_2.txt\n");
            bw2.close();

            BufferedWriter bw3 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("Task_3.txt")));
            Map<String, Integer> activityFrequencyMap = tasks.activityFrequency(activityList);
            Iterator<Entry<String, Integer>> it = activityFrequencyMap.entrySet().iterator();
            bw3.write("Task 3 output : " + "\n");
            System.out.println("Task3 processing ->result in Task_3.txt\n");
            while (it.hasNext()) {
                Map.Entry<String, Integer> pair = it.next();
                String activityName = pair.getKey();
                int numberOfAparition = pair.getValue();

                bw3.write("Activity : " + activityName + " happened " + numberOfAparition + " times!" + "\n");
            }
            bw3.close();

            BufferedWriter bw4 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("Task_4.txt")));
            System.out.println("Task4 processing -> result in Task_4.txt\n");
            bw4.write("Task 4 output : \n");
            Map<Integer, Map<String, Long>> freqOnDays = tasks.activityFrequencyOnDays(activityList);

            for (Entry<Integer, Map<String, Long>> p : freqOnDays.entrySet()) {
                int day = p.getKey();
                Map<String, Long> actMap = p.getValue();

                for (Entry<String, Long> pair : actMap.entrySet()) {
                    String activityName = pair.getKey();
                    Long numberOfOccurances = pair.getValue();

                    bw4.write("DAY : " + day + " activity : " + activityName + " happened " + numberOfOccurances + " times!" + "\n");
                }
            }
            bw4.close();

            BufferedWriter bw5 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("Task_5.txt")));
            System.out.println("Task5 processing ->result in Task_5.txt\n");
            bw5.write("Task 5 output : \n");
            Map<String, String> activityDuration = tasks.activityDuration(activityList);
            for (Entry<String, String> pair : activityDuration.entrySet()) {
                String activityName = pair.getKey();
                String duration = pair.getValue();

                bw5.write("Activity : " + activityName + " with duration : " + duration + "\n");
            }

            bw5.close();

            BufferedWriter bw6 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("Task_6.txt")));
            System.out.println("Task6 processing ->result in Task_6.txt\n");
            bw6.write("Task 6 output : ");

            List<String> taskSixOut = tasks.Task6(activityList);
            for (String currentString : taskSixOut) {

                bw6.write(currentString);
            }
            bw6.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
