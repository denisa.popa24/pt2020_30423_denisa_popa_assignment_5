package bussinesLayer;

public class MonitoredData {
    private MyData startTime;
    private MyData endTime;
    private String activity;

    public MonitoredData(MyData startTime, MyData endTime, String activity) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }

    public MyData getStartTime() {
        return startTime;
    }

    public MyData getEndTime() {
        return endTime;
    }

    public String getActivity() {
        return activity;
    }


}
