package bussinesLayer;

public class MyData {
    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;
    private int second;

    public MyData(int year,int month,int day,int hour,int minute,int second){
        this.year=year;
        this.month=month;
        this.day=day;
        this.hour=hour;
        this.minute=minute;
        this.second=second;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getSecond() {
        return second;
    }

    public String data(){
        StringBuilder s = new StringBuilder();
        s.append(year);
        s.append("-");
        s.append(month);
        s.append("-");
        s.append(day);
        s.append(" ");
        s.append(hour);
        s.append(":");
        s.append(minute);
        s.append(":");
        s.append(second);
        s.append("\t");
        return s.toString();
    }
}


