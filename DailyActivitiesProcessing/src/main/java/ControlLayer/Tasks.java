package ControlLayer;

import bussinesLayer.MonitoredData;
import bussinesLayer.MyData;

import java.util.*;
import java.util.stream.Collectors;

public class Tasks {


    public ArrayList<Integer> countDays(ArrayList<MonitoredData> activityList) {
        Task2 obj = (activity) -> {
            ArrayList<Integer> dayList = new ArrayList<>();

            for (MonitoredData currentActivity : activity) {
                MyData startDate = currentActivity.getStartTime();
                MyData endDate = currentActivity.getEndTime();
                int startDay = startDate.getDay();
                int endDay = endDate.getDay();

                if (!dayList.contains(startDay)) {
                    dayList.add(startDay);
                }
                if (!dayList.contains(endDay)) {
                    dayList.add(endDay);
                }
            }
            return dayList;
        };
        return obj.countDistinctDays(activityList);
    }

    public Map<String, Integer> activityFrequency(ArrayList<MonitoredData> activityList) {
        Task3 obj = (activity) -> {

            ArrayList<String> activityNames = new ArrayList<>();
            Map<String, Integer> activityFrequencyMap = new HashMap<>();
            int cnt;

            for (MonitoredData currentActivity : activity) {
                String currentActString = currentActivity.getActivity();

                Iterator<MonitoredData> i = activity.iterator();

                cnt = 0;
                while (i.hasNext()) {
                    MonitoredData currAct = i.next();
                    if (currAct.getActivity().compareTo(currentActString) == 0)
                        cnt++;
                }

                if (!activityNames.contains(currentActString)) {
                    activityNames.add(currentActString);
                    activityFrequencyMap.put(currentActString, cnt);
                }
            }
            return activityFrequencyMap;

        };
        return obj.ActivityFrequency(activityList);
    }

    public Map<Integer, Map<String, Long>> activityFrequencyOnDays(ArrayList<MonitoredData> activityList) {
        Task4 obj = (activity) -> {
            Map<Integer, Map<String, Long>> frequencyOnDays = activityList.stream().collect(Collectors.groupingBy(element -> {
                int day = element.getStartTime().getDay();
                return day;
            }, Collectors.groupingBy(element -> element.getActivity(), Collectors.counting())));

            return frequencyOnDays;
        };
        return obj.ActivityFrequencyOnDays(activityList);

    }

    public Map<String, String> activityDuration(ArrayList<MonitoredData> activityList) {

        Task5 obj = (activity) -> {

            Map<String, Integer> activDuration = new HashMap<>();
            ArrayList<String> activityNames = new ArrayList<>();

            Iterator<MonitoredData> i = activity.iterator();
            int sum;
            while (i.hasNext()) {
                MonitoredData curentMonitoredData = i.next();
                String activityName = curentMonitoredData.getActivity();
                sum = 0;
                for (MonitoredData currAct : activity) {
                    String activityNameString = currAct.getActivity();
                    if (activityName.compareTo(activityNameString) == 0) {

                        long start = (currAct.getStartTime().getYear() - 2010) * 365 * 24 * 60 * 60
                                + (currAct.getStartTime().getMonth() - 1) * 30 * 24 * 60 * 60
                                + (currAct.getStartTime().getDay() - 1) * 24 * 60 * 60
                                + (currAct.getStartTime().getHour()) * 60 * 60
                                + (currAct.getStartTime().getMinute()) * 60
                                + (currAct.getStartTime().getSecond());
                        long end = (currAct.getEndTime().getYear() - 2010) * 365 * 24 * 60 * 60
                                + (currAct.getEndTime().getMonth() - 1) * 30 * 24 * 60 * 60
                                + (currAct.getEndTime().getDay() - 1) * 24 * 60 * 60
                                + (currAct.getEndTime().getHour()) * 60 * 60
                                + (currAct.getEndTime().getMinute()) * 60
                                + (currAct.getEndTime().getSecond());
                        long durat = end - start;
                        sum += durat;
                    }
                }
                if (!activityNames.contains(activityName)) {
                    activityNames.add(activityName);
                    activDuration.put(activityName, sum);
                }
            }

            Map<String, String> actDurationString = new HashMap<>();
            for (Map.Entry<String, Integer> pair : activDuration.entrySet()) {
                String aName = pair.getKey();
                int dur = pair.getValue();

                int days = dur / 60 / 60 / 24;
                int hours = (dur - (days * 24 * 60 * 60)) / 60 / 60;
                int minutes = (dur - (days * 24 * 60 * 60) - hours * 60 * 60) / 60;
                int seconds = (dur - (days * 24 * 60 * 60) - hours * 60 * 60 - minutes * 60);

                StringBuilder sb = new StringBuilder();//format days- hours:minutes:seconds
                sb.append(days + "-");
                sb.append(hours + ":");
                sb.append(minutes + ":");
                sb.append(seconds);
                if (dur >0)
                    actDurationString.put(aName, sb.toString());
            }

            return actDurationString;
        };
        return obj.activityDuration(activityList);
    }

    public List<String> Task6(ArrayList<MonitoredData> activityList) {

        Map<String, Integer> actFreq = activityFrequency(activityList);
        Map<String, Long> lessThan5Mins = activityList.stream().filter(element -> {
            long start = (element.getStartTime().getYear() - 2010) * 365 * 24 * 60 * 60
                    + (element.getStartTime().getMonth() - 1) * 30 * 24 * 60 * 60
                    + (element.getStartTime().getDay() - 1) * 24 * 60 * 60
                    + (element.getStartTime().getHour()) * 60 * 60
                    + (element.getStartTime().getMinute()) * 60
                    + (element.getStartTime().getSecond());
            long end = (element.getEndTime().getYear() - 2010) * 365 * 24 * 60 * 60
                    + (element.getEndTime().getMonth() - 1) * 30 * 24 * 60 * 60
                    + (element.getEndTime().getDay() - 1) * 24 * 60 * 60
                    + (element.getEndTime().getHour()) * 60 * 60
                    + (element.getEndTime().getMinute()) * 60
                    + (element.getEndTime().getSecond());
            long dure = end - start;
            if (dure < 5 * 60) {
                return true;
            } else
                return false;
        }).collect(Collectors.groupingBy(element -> element.getActivity(), Collectors.counting()));

        List<String> tSixList = activityList.stream().filter(element -> {
            if (lessThan5Mins.get(element.getActivity()) == null) {
                return false;
            } else if (lessThan5Mins.get(element.getActivity()) >= 0.9 * actFreq.get(element.getActivity())) {
                return true;
            } else {

                return false;
            }
        }).map(monitoredData -> monitoredData.getActivity()).distinct().collect(Collectors.toList());
        return tSixList;
    }
}