package ControlLayer;

import bussinesLayer.MonitoredData;

import java.util.ArrayList;
import java.util.Map;

public interface Task3 {

    Map<String, Integer> ActivityFrequency(ArrayList<MonitoredData> activityList);
}
