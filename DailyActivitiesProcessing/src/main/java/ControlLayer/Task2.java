package ControlLayer;

import bussinesLayer.MonitoredData;

import java.util.ArrayList;

public interface Task2 {

    ArrayList<Integer> countDistinctDays(ArrayList<MonitoredData> activityList);
}
