package ControlLayer;

import bussinesLayer.MonitoredData;

import java.util.ArrayList;
import java.util.Map;

public interface Task4 {

    Map<Integer, Map<String, Long>> ActivityFrequencyOnDays(ArrayList<MonitoredData> activityList);
}
