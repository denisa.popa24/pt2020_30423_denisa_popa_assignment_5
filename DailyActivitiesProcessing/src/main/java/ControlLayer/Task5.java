package ControlLayer;

import bussinesLayer.MonitoredData;

import java.util.ArrayList;
import java.util.Map;

public interface Task5 {

    Map<String, String> activityDuration(ArrayList<MonitoredData> activityList);
}
